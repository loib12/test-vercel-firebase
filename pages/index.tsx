import Head from 'next/head'
import { useEffect } from 'react';
import styles from '../styles/Home.module.scss'
import { collection, addDoc, getDocs } from 'firebase/firestore';
import { app, database } from '../firebaseConfig';
import { useRouter } from 'next/router';
import { useAuthState } from "react-firebase-hooks/auth";
import { auth, logout } from '../Auth'

const dbInstance = collection(database, 'notes');

export default function Home() {
  const router = useRouter()
  const [user, loading, error] = useAuthState(auth);
  const saveNote = () => {
    addDoc(dbInstance, {
      noteTitle: 'Nguyen Huu Loi 2'
    })
  }
  const getNotes = () => {
    getDocs(dbInstance)
      .then((data) => {
        console.log(data.docs.map((item) => {
          return { ...item.data(), id: item.id }
        }));
      })
  }
  useEffect(() => {
    getNotes();
}, [])
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <button
        onClick={saveNote}>
        <h1>Save</h1>
      </button>
      {
        !user && <button onClick={() => router.push('/login')}>login</button>
      }
      {
        user && <button onClick={logout}>logout</button>
      }

    </div>
  )
}
