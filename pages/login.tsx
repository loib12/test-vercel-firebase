import React, { useEffect, useState } from 'react';
import { signInWithGoogle, signInWithFacebook, auth, requestOTP } from '../Auth'
import { useAuthState } from "react-firebase-hooks/auth";
import { useRouter } from 'next/router';
import {
  RecaptchaVerifier,
} from "firebase/auth";
import {
  ConfirmationResult,
} from "firebase/auth";

// extend window interface
declare global {
  interface Window {
    recaptchaCerifier: RecaptchaVerifier;
  }
}

function Login() {
  const router = useRouter();
  const [user, loading, error] = useAuthState(auth);
  const [otp, setOtp] = useState('');
  const [phone, setPhone] = useState('+84974830103')
  const [isExpand, setIsExpand] = useState(false)
  const [confirmationResult, setConfirmationResult] = useState<ConfirmationResult>();

  const signInWithPhoneNumber = () => {
    requestOTP(phone, setConfirmationResult)
    setIsExpand(true)
  }
  const verifyOTP = () => {
    if (otp === null) return;
    if (confirmationResult) {
      confirmationResult.confirm(otp).catch((err) => {alert(err)})
    }
  }

  useEffect(() => {
    if (loading) return;
    if (user) router.push('/');
  }, [user, loading]);

  return (
    <div style={{ padding: 20 }}>
      <h1>{process.env.TEST_ENV}</h1>
      <div style={{ display: 'flex' }}>
        <input name="phone-number" value={phone} onChange={(e) => setPhone(e.target.value)} />
        <button onClick={signInWithPhoneNumber}>login with phone number</button>
        <br></br>
        <div id="recaptcha-container"></div>
      </div>
      {
        isExpand && (
          <div style={{ display: 'flex' }}>
            <input name="otp" value={otp} onChange={(e) => setOtp(e.target.value)} />
            <button onClick={verifyOTP}>Verify</button>
            <br></br>
            <div id="recaptcha-container"></div>
          </div>
        )
      }
      <button className="login__btn login__google" onClick={signInWithGoogle}>
        Login with Google
      </button>
      <button onClick={signInWithFacebook}>
        Login with Facebook
      </button>
    </div>
  );
}

export default Login;
